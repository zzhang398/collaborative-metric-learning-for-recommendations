#Collaborative filtering with implicit feedback 

This project is for COMP597 course in Mcgill in Winter, 2020.
Some common models for this problem are collected. Particularly, more attention
has been paid on metric learning-based approaches and more effort were spended on 
improving the existing models with adaptive margins.


To test the source code. One need to install Tensorflow 2.0 and Keras first.
Then go to the "test" folder, run the source code: python test_item_ranking.py


