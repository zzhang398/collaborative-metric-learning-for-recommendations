# -*- coding: utf-8 -*-
from __future__ import unicode_literals
#import tensorflow as tf
import time
import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 

from utils.evaluation.RankingMetrics import *

class SML(object):
    def __init__(self, sess = tf.Session(), num_user=0, num_item=0, learning_rate=0.1, reg_rate=0.1, epoch=500, batch_size=500,
                 verbose=False, t=5, display_step=1000,lamb_da=10,gamma=0.1):
        self.sess = sess
        self.num_user = num_user
        self.num_item = num_item
        self.learning_rate = learning_rate
        self.reg_rate = reg_rate
        self.epochs = epoch
        self.batch_size = batch_size
        self.verbose = verbose
        self.T = t
        self.display_step = display_step

        self.user_id = None
        self.item_id = None
        self.neg_item_id = None
        self.keep_rate = None
        self.pred_distance = None
        self.pred_distance_neg = None
        self.loss = None
        self.optimizer = None
        self.clip_P = None
        self.clip_Q = None

        self.user = None
        self.item = None
        self.num_training = None
        self.test_data = None
        self.total_batch = None
        self.neg_items = None
        self.test_users = None
        self.lamb_da =lamb_da
        self.gamma=gamma
        print("You are running SML.")

    def build_network(self, num_factor=100, margin=0.5, norm_clip_value=1):

        self.user_id = tf.placeholder(dtype=tf.int32, shape=[None], name='user_id')
        self.item_id = tf.placeholder(dtype=tf.int32, shape=[None], name='item_id')
        self.neg_item_id = tf.placeholder(dtype=tf.int32, shape=[None], name='neg_item_id')
        self.keep_rate = tf.placeholder(tf.float32)
     
        P = tf.Variable(
            tf.random_normal([self.num_user, num_factor], stddev=1 / (num_factor ** 0.5)), dtype=tf.float32)
        Q = tf.Variable(
            tf.random_normal([self.num_item, num_factor], stddev=1 / (num_factor ** 0.5)), dtype=tf.float32)

        user_embedding = tf.nn.embedding_lookup(P, self.user_id)
        item_embedding = tf.nn.embedding_lookup(Q, self.item_id)
        neg_item_embedding = tf.nn.embedding_lookup(Q, self.neg_item_id)
        
        #self.neg_prdid = tf.placeholder(tf.int32,[None,self.neg_samples],name='neg_prdid')
        #self.neg_userid = tf.placeholder(tf.int32,[None,self.neg_samples],name='neg_userid')
        # B =tf.cast(self.user_neighbours,dtype=tf.float32)
        
        with tf.name_scope(name="B"):
            B= tf.Variable(np.array([1.0]*self.num_user), dtype=tf.float32,trainable=True)
            B1=tf.Variable(np.array([1.0]*self.num_item),dtype=tf.float32,trainable=True)
        bias = tf.nn.embedding_lookup(B,self.user_id) 
        
        user_embedding = tf.nn.embedding_lookup(P, self.user_id)
        pbias= tf.nn.embedding_lookup(B1,self.item_id)
        item_embedding = tf.nn.embedding_lookup(Q, self.item_id)
        neg_item_embedding = tf.nn.embedding_lookup(Q, self.neg_item_id)
        print(bias)

        #neg_prd_embedding = tf.nn.embedding_lookup(P,tf.reshape(self.neg_prdid,[-1]))
        #u_temp = tf.reshape(tf.tile(user_embedding,[1,self.neg_samples]),[-1,self.hidden_size] )
        #p_temp_neg = tf.reshape(neg_prd_embedding, [-1,self.hidden_size] )
        #p_temp = tf.reshape(tf.tile(prd_embedding,[1,self.neg_samples]),[-1,self.hidden_size] )

        self.pred_distance =  tf.reduce_sum(tf.square(user_embedding-item_embedding),1) 
        self.pred_distance_neg = tf.reduce_sum(tf.multiply( user_embedding- neg_item_embedding ,user_embedding- neg_item_embedding),1) 
        self.pred_distance_PN =  tf.reduce_sum(tf.multiply(item_embedding-neg_item_embedding,item_embedding- neg_item_embedding ),1) 
        #self.pred_distance_neg =tf.reduce_sum(tf.square(u_temp-p_temp_neg),1) #tf.reduce_mean(tf.reduce_sum(tf.square(distance),2),1)

        # a =tf.reduce_sum(tf.log(tf.exp(self.margin)+tf.exp(self.pred_distance-self.pred_distance_neg)))
        # b = tf.reduce_sum(tf.log(tf.exp(self.margin)+tf.exp(self.pred_distance- self.pred_distance_PN)))

        a = tf.maximum(self.pred_distance - self.pred_distance_neg + bias, 0)
        b = tf.maximum(self.pred_distance-self.pred_distance_PN+pbias,0)
        #b = tf.maximum(self.pred_distance_PN-pbias,0)
        
        #whole model
        self.loss= tf.reduce_sum(a)+self.lamb_da*tf.reduce_sum(b)
        self.loss=self.loss-1*(self.gamma*(tf.reduce_mean(bias) +tf.reduce_mean( pbias)))

        tf.add_to_collection('user_embedding',user_embedding)
        tf.add_to_collection('item_embedding',item_embedding)

        self.clip_P = tf.assign(P, tf.clip_by_norm(P, 1.0, axes=[1]))
        self.clip_Q = tf.assign(Q, tf.clip_by_norm(Q, 1.0, axes=[1]))
        self.clip_B = tf.assign(B,tf.clip_by_value(B,0,1.0))
        self.clip_B1=tf.assign(B1,tf.clip_by_value(B1,0,1.0))
        
        self.optimizer = tf.train.AdagradOptimizer(self.learning_rate).minimize(self.loss, var_list=[P, Q])

        return self
 
 
    def prepare_data(self, train_data, test_data):
        """
        You must prepare the data before train and test the model
        :param train_data:
        :param test_data:
        :return:
        """
        t = train_data.tocoo()
        self.user = t.row.reshape(-1)
        self.item = t.col.reshape(-1)
        self.num_training = len(self.item)
        self.test_data = test_data
        self.total_batch = int(self.num_training / self.batch_size)
        self.neg_items = self._get_neg_items(train_data.tocsr())
        self.test_users = set([u for u in self.test_data.keys() if len(self.test_data[u]) > 0])
        print(self.total_batch)
        print("data preparation finished.")

    def train(self):
        idxs = np.random.permutation(self.num_training)  # shuffled ordering
        user_random = list(self.user[idxs])
        item_random = list(self.item[idxs])
        item_random_neg = []
        for u in user_random:
            neg_i = self.neg_items[u]
            s = np.random.randint(len(neg_i))
            item_random_neg.append(neg_i[s])

        # train
        for i in range(self.total_batch):
            start_time = time.time()
            batch_user = user_random[i * self.batch_size:(i + 1) * self.batch_size]
            batch_item = item_random[i * self.batch_size:(i + 1) * self.batch_size]
            batch_item_neg = item_random_neg[i * self.batch_size:(i + 1) * self.batch_size]

            _, loss, _, _ = self.sess.run((self.optimizer, self.loss, self.clip_P, self.clip_Q),
                                          feed_dict={self.user_id: batch_user, self.item_id: batch_item, self.neg_item_id: batch_item_neg, self.keep_rate: 0.98})

            if i % self.display_step == 0:
                if self.verbose:
                    print("Index: %04d; cost= %.9f" % (i + 1, np.mean(loss)))
                    print("one iteration: %s seconds." % (time.time() - start_time))


    def test(self):
        evaluate(self)

    def execute(self, train_data, test_data):

        self.prepare_data(train_data, test_data)

        init = tf.global_variables_initializer()
        self.sess.run(init)

        for epoch in range(self.epochs):
            self.train()
            if (epoch+1) % self.T == 0:
                print("Epoch: %04d; " % epoch, end='')
                self.test()

    def save(self, path):
        saver = tf.train.Saver()
        saver.save(self.sess, path)

    def predict(self, user_id, item_id):
        return -self.sess.run([self.pred_distance],
                              feed_dict={self.user_id: user_id, self.item_id: item_id, self.keep_rate: 1})[0]

    def _get_neg_items(self, data):
        all_items = set(np.arange(self.num_item))
        neg_items = {}
        for u in range(self.num_user):
            neg_items[u] = list(all_items - set(data.getrow(u).nonzero()[1]))

        return neg_items