# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 18:44:17 2020

@author: root
"""

import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 
import numpy as np

# x = tf.ones((2, 2))

# with tf.GradientTape() as t:
#   t.watch(x)
#   y = tf.reduce_sum(x)
#   z = tf.multiply(y, y)

# # Derivative of z with respect to the original input tensor x
# dz_dx = t.gradient(z, x)
# for i in [0, 1]:
#   for j in [0, 1]:
#     assert dz_dx[i][j].numpy() == 8.0
# print(dz_dx)
print(type(tf.Session().run(tf.constant([1,2,3]))))
tf.Session().close()
for i in range(3):
    var = tf.Variable(0)
    sess = tf.Session(config=tf.ConfigProto())
    with sess.as_default():
        tf.global_variables_initializer().run()
        print(len(sess.graph._nodes_by_name.keys()))
    sess.close() 

# import tensorflow as tf

# @tf.function
# def square_if_positive(x):
#   if x > 0:
#     x = x * x
#   else:
#     x = 0
#   return x


# print('square_if_positive(2) = {}'.format(square_if_positive(tf.constant(2))))
# print('square_if_positive(-2) = {}'.format(square_if_positive(tf.constant(-2))))


# @tf.function 
# def f(x): 
#   if tf.reduce_sum(x) > 0: 
#     return x * x 
#   else: 
#     return -x // 2 

# f(tf.constant(-2))
# print(.format(f(tf.constant(2))))