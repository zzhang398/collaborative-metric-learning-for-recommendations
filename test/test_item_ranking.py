import argparse
import tensorflow as tf
import sys
import os.path
#reference path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
# from models.item_ranking.cdae import ICDAE
# from models.item_ranking.bprmf import BPRMF
tf.compat.v1.reset_default_graph()
from models.item_ranking.cml import CML
# from models.item_ranking.neumf import NeuMF
# from models.item_ranking.gmf import GMF
from models.item_ranking.lrml import LRML
from models.item_ranking.cdae import ICDAE
# from models.item_ranking.mlp import MLP
# from models.item_ranking.jrl import JRL
from models.item_ranking.A_CML import A_CML
from models.item_ranking.sml import SML

from utils.load_data.load_data_ranking import *
from utils.load_data.dataset import *


def parse_args():
    parser = argparse.ArgumentParser(description='DeepRec')
    parser.add_argument('--model', choices=['CDAE', 'CML', 'LRML', 'A_CML', 'SML'],
                        default='SML')
    parser.add_argument('--epochs', type=int, default=25)
    parser.add_argument('--num_factors', type=int, default=5)
    parser.add_argument('--display_step', type=int, default=1000)
    parser.add_argument('--batch_size', type=int, default=1024)  # 128 for unlimpair
    parser.add_argument('--trainset_flag', type=int, default=1)    
    parser.add_argument('--learning_rate', type=float, default=1e-3)  # 1e-4 for unlimpair
    parser.add_argument('--reg_rate', type=float, default=0.1)  # 0.01 for unlimpair
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    epochs = args.epochs
    learning_rate = args.learning_rate
    reg_rate = args.reg_rate
    num_factors = args.num_factors
    display_step = args.display_step
    batch_size = args.batch_size
    trainset_flag = args.trainset_flag
    #change the original file into TXT format, into four columns
    #input file path
    if trainset_flag == 0:
        pathin = "../data/aiv5/Amazon_Instant_Video_5.json.gz"
        pathout= "../data/aiv5/dataset.txt"
        read(pathin,pathout)
        print("You are using the Amazon Instant Video dataset")
    else:
        pathout="../data/ml100k/movielens_100k.dat"
        print("You are using the Movienlens 100k dataset")
    # pathout= "../data/aiv5/dataset.txt"
    # path=pathout
    train_data, test_data, n_user, n_item = load_data_neg(path=pathout,test_size=0.1, sep="\t")

    try:
        gpus = tf.config.experimental.list_physical_devices('GPU')
        tf.config.experimental.set_memory_growth(gpus[0], True)
    except:
        # Invalid device or cannot modify virtual devices once initialized.
        pass

    model = None
    # Model selection
    if args.model == "CDAE":
        train_data, test_data, n_user, n_item = load_data_all(path=pathout, test_size=0.1, sep="\t")
        # model = ICDAE(n_user, n_item)
        model = ICDAE(num_user=n_user, num_item=n_item, epoch=epochs)
    if args.model == "CML":
        model = CML(num_user=n_user, num_item=n_item, epoch=epochs)
    if args.model == "LRML":
        model = LRML(num_user=n_user, num_item=n_item, epoch=epochs)
    if args.model == "A_CML":
        model = A_CML(num_user=n_user, num_item=n_item, epoch=epochs)        
    if args.model == "SML":
        model = SML(num_user=n_user, num_item=n_item, epoch=epochs)    
        #model = LRML(n_user, n_item)
    
    # build and execute the model
    if model is not None:
        model.build_network()
        model.execute(train_data, test_data)
