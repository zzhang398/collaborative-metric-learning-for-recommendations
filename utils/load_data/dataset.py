# -*- coding: utf-8 -*-
"""
Spyder Editor

This program reads 'Amazon_Instant_Video_5.json.gz' into 'dataset.txt'

"""

import json
import gzip

def parse(path):
  g = gzip.open(path, 'r')
  for l in g:
    yield json.dumps(eval(l))
  

def read(pathin,pathout):
    #the file location of 'Amazon_Instant_Video_5.json.gz'
    f = open(pathout, 'w+')
    for l in parse(pathin):
        l = eval(l)
        f.write(l["reviewerID"]+'\t'+l["asin"]+'\t'+str(l["overall"]) + '\t' + str(l["unixReviewTime"])+'\n')
    f.close()
# import json
# import gzip

# def parse(path):
  
#   for l in g:
#     yield eval(l)

# path = "C:/Users/root/Desktop/SML-master/data"
# f = open("temp.txt", 'a+')
# g = gzip.open(path+"/Amazon_Instant_Video_5.json.gz", 'r')
# for l in g:
#     if l==0:
#         print(l)
#     f.write(l["reviewerID"]+'\t\t'+l["asin"]+'\t\t'+l["overall"] + l["unixReviewTime"]+'\n')

# f.close()
    # f.write(l + '\n')

