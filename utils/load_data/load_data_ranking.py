import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from scipy.sparse import csr_matrix

# #convert user_id and item_id into numbers:
def conversion(df):
    u_dict = dict()
    flag = 0
    for i in df.index:
        user = df.loc[i,'user_id']
        if user not in u_dict:
            flag += 1
            u_dict[user] = flag
        df.loc[i,'user_id'] = u_dict[user]
    
    i_dict = dict()
    flag = 0
    for j in df.index:
        item = df.loc[j,'item_id']
        if item not in i_dict:
            flag += 1
            i_dict[item] = flag
        df.loc[j,'item_id'] = i_dict[item]
    
    return df, u_dict, i_dict
    
def back_conversion(df, u_dict, i_dict):
    for word in df.user_id:
        df.loc[word,'user_id'] = {v:k for k, v in u_dict.items()}[word]
    for word in df.item_id:
        df.loc[word,'item_id'] = {v:k for k, v in i_dict.items()}[word]
    return df
    
    
def load_data_neg(path="../data/ml100k/movielens_100k.dat", header=['user_id', 'item_id', 'rating', 'category'],
                  test_size=0.1, sep="\t"):
    df = pd.read_csv(path, sep=sep, names=header, engine='python')
    
    if type(df.user_id[0])!=int or type(df.item_id[0])!=int:
        df, u_dict, i_dict = conversion(df)
        
    n_users = df.user_id.unique().shape[0]
    n_items = df.item_id.unique().shape[0]


    train_data, test_data = train_test_split(df, test_size=test_size)
    train_data = pd.DataFrame(train_data)
    test_data = pd.DataFrame(test_data)

    train_row = []
    train_col = []
    train_rating = []

    for line in train_data.itertuples():
        u = line[1] - 1
        i = line[2] - 1
        train_row.append(u)
        train_col.append(i)
        train_rating.append(1)
    train_matrix = csr_matrix((train_rating, (train_row, train_col)), shape=(n_users, n_items))

    # all_items = set(np.arange(n_items))
    # neg_items = {}
    # for u in range(n_users):
    #     neg_items[u] = list(all_items - set(train_matrix.getrow(u).nonzero()[1]))

    test_row = []
    test_col = []
    test_rating = []
    for line in test_data.itertuples():
        test_row.append(line[1] - 1)
        test_col.append(line[2] - 1)
        test_rating.append(1)
    test_matrix = csr_matrix((test_rating, (test_row, test_col)), shape=(n_users, n_items))

    test_dict = {}
    for u in range(n_users):
        test_dict[u] = test_matrix.getrow(u).nonzero()[1]

    print("Load data finished. Number of users:", n_users, "Number of items:", n_items)
    return train_matrix.todok(), test_dict, n_users, n_items



def load_data_all(path="../data/ml100k/movielens_100k.dat", header=['user_id', 'item_id', 'rating', 'time'],
                  test_size=0.1, sep="\t"):
    df = pd.read_csv(path, sep=sep, names=header, engine='python')

    if type(df.user_id[0])!=int or type(df.item_id[0])!=int:
        df, u_dict, i_dict = conversion(df)
        
    n_users = df.user_id.unique().shape[0]
    n_items = df.item_id.unique().shape[0]

    train_data, test_data = train_test_split(df, test_size=test_size)
    train_data = pd.DataFrame(train_data)
    test_data = pd.DataFrame(test_data)

    train_row = []
    train_col = []
    train_rating = []

    train_dict = {}
    for line in train_data.itertuples():
        u = line[1] - 1
        i = line[2] - 1
        train_dict[(u, i)] = 1

    for u in range(n_users):
        for i in range(n_items):
            train_row.append(u)
            train_col.append(i)
            if (u, i) in train_dict.keys():
                train_rating.append(1)
            else:
                train_rating.append(0)
    train_matrix = csr_matrix((train_rating, (train_row, train_col)), shape=(n_users, n_items))
    all_items = set(np.arange(n_items))

    neg_items = {}
    train_interaction_matrix = []
    for u in range(n_users):
        neg_items[u] = list(all_items - set(train_matrix.getrow(u).nonzero()[1]))
        train_interaction_matrix.append(list(train_matrix.getrow(u).toarray()[0]))

    test_row = []
    test_col = []
    test_rating = []
    for line in test_data.itertuples():
        test_row.append(line[1] - 1)
        test_col.append(line[2] - 1)
        test_rating.append(1)
    test_matrix = csr_matrix((test_rating, (test_row, test_col)), shape=(n_users, n_items))

    test_dict = {}
    for u in range(n_users):
        test_dict[u] = test_matrix.getrow(u).nonzero()[1]

    print("Load data finished. Number of users:", n_users, "Number of items:", n_items)

    return train_interaction_matrix, test_dict, n_users, n_items